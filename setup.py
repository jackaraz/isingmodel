import setuptools

with open("README.md", "r", encoding="utf-8") as f:
    long_description = f.read()

with open("requirements.txt", "r") as f:
    requirements = f.read()
requirements = [x for x in requirements.split("\n") if x != ""]


setuptools.setup(
    name="IsingModel",
    version="0.0.1",
    description=("Ising Model"),
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/jackaraz/isingmodel",
    project_urls={
        "Bug Tracker": "https://gitlab.com/jackaraz/isingmodel/-/issues",
    },
    author="J.Y. Araz",
    author_email="jack.araz@durham.ac.uk",
    license="MIT",
    install_requires=requirements,
    python_requires=">=3.6",
    package_dir={"": "src"},
    packages=setuptools.find_packages(where="src"),
    classifiers=[
        "Intended Audience :: Science/Research",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
        "Programming Language :: Python :: 3",
        "Topic :: Scientific/Engineering :: Physics",
    ],
)
