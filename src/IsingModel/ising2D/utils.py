import numpy as np
from numba import njit, prange, jit

from typing import List, Any, Union, Tuple, Text, Dict, Optional, Callable

def InitializeRandomState(shape: Union[int, List[int]]) -> np.ndarray:
    """
    Initialize a random spin state where -1 is spin down and 1 is spin up
    Parameters
    ----------
    shape : Union[int, List[int]]
        shape of the spin state

    Returns
    -------
    np.ndarray
        random spin state
    """
    return np.random.choice(
        [-1.,1.], size = tuple(shape) if isinstance(shape, (list,tuple)) else (shape, shape)
    )

@njit
def MCMCstep(configuration: np.ndarray, invkT: float, J: float = 1.) -> np.ndarray:
    """
    Run a single MCMC step on given spin configuration

    Parameters
    ----------
    configuration : np.ndarray
        2D spin configuration
    invkT : float
        1/kT
    J : float
        exchange constant

    Returns
    -------
    np.ndarray
        Updated spin configuration
    """
    L, W = configuration.shape
    for i in prange(L):
        for j in prange(W):
            # choose a random spin from the configuration space
            x, y = int(np.random.randint(0,L)), int(np.random.randint(0,W))
            spin = configuration[x,y]

            # collect the nearest neighbour spins in the latice (up, down, left, right)
            neighbours = J*(configuration[(x+1)%L, y] + configuration[x, (y+1)%W] \
                            + configuration[(x-1)%L, y] + configuration[x, (y-1)%W])

            # Energy change if spin[x,y] is flopped, according to the surrounding spins
            energyCost = 2.*spin*neighbours

            # accept the move if the change in energy is negative
            if energyCost < 0.:
                spin *= -1
            elif np.random.rand() < np.exp(-energyCost * invkT):
                spin *= -1

            configuration[x,y] = spin

    return configuration


@jit(forceobj=True)
def calculateEnergy(configuration: np.ndarray, J: float = 1.) -> np.ndarray:
    """
    Calculate the energy of a given spin configuration

    Parameters
    ----------
    configuration : np.ndarray
        2D spin configuration
    J : float
        exchange constant

    Returns
    -------
    np.ndarray
        energy of the spin configuration
    """
    energy = np.array(0.)
    L, W = configuration.shape
    for i in prange(L):
        for j in prange(W):
            spinEnergy = configuration[i,j]
            neighbours = J*(configuration[(i+1)%L, j] + configuration[i, (j+1)%W] \
                            + configuration[(i-1)%L, j] + configuration[i, (j-1)%W])
            energy += -spinEnergy*neighbours
    return energy/4.


@jit(forceobj=True)
def calculateMagnetization(configuration: np.ndarray) -> np.ndarray:
    """
    Calculate the magnetization of a given spin configuration

    Parameters
    ----------
    configuration : np.ndarray
        2D spin configuration

    Returns
    -------
    np.ndarray
        magnetization of the spin configuration
    """
    return np.sum(configuration)


@njit
def simulate(
        configuration: np.ndarray, initial_invkT: float, J : float, evolutionSteps: int
) -> np.ndarray:
    """
    Simulate MCMC for 2D Ising model

    Parameters
    ----------
    configuration : np.ndarray
        2D spin configuration
    initial_invkT : float
        1/kT
    J : float
        exchange constant
    evolutionSteps : int
        steps of evolution

    Returns
    -------
    np.ndarray
        all the states that are generated.
        shape (evolutionSteps, configuration.shape[0], configuration.shape[1])
    """
    init = configuration.copy()
    states = [init]
    for ix in prange(evolutionSteps):
        states.append(MCMCstep(configuration, initial_invkT).copy())
    return states